
using UnityEngine;

public abstract class Hittable : MonoBehaviour
{
    public abstract void OnHit(Vector3 hitPoint, Vector3 normal);
}
