using UnityEngine;

public class StaticHittable : Hittable
{
	[SerializeField]
	[Tooltip("Particle system index in the particle manager")]
	private int _particleSystemIndex;

	public override void OnHit(Vector3 hitPoint, Vector3 normal)
	{
		ParticleSystem ps = HitParticleManager.Get(_particleSystemIndex);
		ps.transform.position = hitPoint;
		ps.transform.forward = normal;
		ps.Emit(1);
	}
}
