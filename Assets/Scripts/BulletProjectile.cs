
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BulletProjectile : MonoBehaviour
{
    [SerializeField]
    private float _speed = 10.0f;

    private Rigidbody _rigidBody;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        _rigidBody.velocity = transform.forward * _speed;
        Destroy(gameObject, 5f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Hittable>(out Hittable hittable))
        {
			Ray ray = new Ray(transform.position, transform.forward);
			if (Physics.Raycast(ray, out RaycastHit hit, 999f, 1))
                hittable.OnHit(hit.point, hit.normal);                
        }

        Destroy(gameObject);
    }
}
