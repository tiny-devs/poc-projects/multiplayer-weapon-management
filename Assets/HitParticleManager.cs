
using UnityEngine;

public class HitParticleManager : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem[] _pfParticleSystems;

	private ParticleSystem[] _particleSystems;

    private static HitParticleManager Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        _particleSystems = new ParticleSystem[_pfParticleSystems.Length];

        for (int i = 0; i < _particleSystems.Length; ++i)
            _particleSystems[i] = Instantiate(_pfParticleSystems[i]).GetComponent<ParticleSystem>();
    }

    public static ParticleSystem Get(int index)
    {
        return Instance._particleSystems[index];
    }
}
